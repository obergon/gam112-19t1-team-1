﻿using System;
using UnityEngine;

namespace TowerDefense.Energy.Data
{
	/// <summary>
	/// SM: S/O describing an instance of a type of Energy, used by Agents (DamageBehaviour) and TowerLevels (Launcher) for net damage calculations
	/// </summary>
	[Serializable]
	[CreateAssetMenu(fileName = "EnergyType", menuName = "TowerDefense/Energy Type", order = 1)]
	public class EnergyType : ScriptableObject
	{
		/// <summary>
		/// The id - used in persistence
		/// </summary>
		public string id;
		
		/// <summary>
		/// Sequence in 'circular' energy damage concept, if needed
		/// </summary>
		public int sequence;

		/// <summary>
		/// The short energy name
		/// </summary>
		public string energyName;

		/// <summary>
		/// The long description of the energy type
		/// </summary>
		public string energyDesc;

		/// <summary>
		/// Raw form of energy
		/// </summary>
		public EnergyForm energyForm;

		/// <summary>
		/// The energy icon
		/// </summary>
		public Sprite energyIcon;

		/// <summary>
		/// Colour used to represent this energy type in UI / particle systems
		/// </summary>
		public Color energyColour;

		/// <summary>
		/// Material used to represent this energy type
		/// </summary>
		public Material energyMaterial;
		
		///// <summary>
		///// Particle system used for firing projectiles
		///// </summary>
		//public ParticleSystem fireParticles;
		
		///// <summary>
		///// Particle system used for expiry
		///// </summary>
		//public ParticleSystem explodeParticles;
	}


	/// <summary>
	/// SM: Raw forms of energy - 'wrapped' by EnergyTypes
	/// </summary>
	public enum EnergyForm
	{
		Default = 0,
		Light = 1,
		Qantm = 2,
		Void = 3,
		Electric = 4,
		Heat = 5
	}
}