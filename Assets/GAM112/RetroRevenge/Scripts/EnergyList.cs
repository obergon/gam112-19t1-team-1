﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TowerDefense.Energy.Data
{
	/// <summary>
	/// SM: Scriptable object for a list of Energy types
	/// </summary>
	[CreateAssetMenu(fileName = "EnergyList", menuName = "TowerDefense/Energy Type List", order = 1)]
	public class EnergyList : ScriptableObject, IList<EnergyType>,
	                         IDictionary<string, EnergyType>, 
	                         ISerializationCallbackReceiver
	{
		public EnergyType[] energyTypes;

		/// <summary>
		/// Cached dictionary of energy types by their IDs
		/// </summary>
		IDictionary<string, EnergyType> m_EnergyDictionary;

		/// <summary>
		/// Gets the number of energy types
		/// </summary>
		public int Count
		{
			get { return energyTypes.Length; }
		}

		/// <summary>
		/// Energy type list is always read-only
		/// </summary>
		public bool IsReadOnly
		{
			get { return true; }
		}

		/// <summary>
		/// Gets an energy type by index
		/// </summary>
		public EnergyType this[int i]
		{
			get { return energyTypes[i]; }
		}

		/// <summary>
		/// Gets an energy type by id
		/// </summary>
		public EnergyType this[string key]
		{
			get { return m_EnergyDictionary[key]; }
		}

		/// <summary>
		/// Gets a collection of all energy type keys
		/// </summary>
		public ICollection<string> Keys
		{
			get { return m_EnergyDictionary.Keys; }
		}

		/// <summary>
		/// Gets the index of a given energy type
		/// </summary>
		public int IndexOf(EnergyType item)
		{
			if (item == null)
			{
				return -1;
			}

			for (int i = 0; i < energyTypes.Length; ++i)
			{
				if (energyTypes[i] == item)
				{
					return i;
				}
			}

			return -1;
		}

		/// <summary>
		/// Gets whether this energy type exists in the list
		/// </summary>
		public bool Contains(EnergyType item)
		{
			return IndexOf(item) >= 0;
		}

		/// <summary>
		/// Gets whether an energy type of the given id exists
		/// </summary>
		public bool ContainsKey(string key)
		{
			return m_EnergyDictionary.ContainsKey(key);
		}

		/// <summary>
		/// Try get energy type with the given key
		/// </summary>
		public bool TryGetValue(string key, out EnergyType value)
		{
			return m_EnergyDictionary.TryGetValue(key, out value);
		}

		// Explicit interface implementations
		// Serialization listeners to create dictionary
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			m_EnergyDictionary = energyTypes.ToDictionary(l => l.id);
		}
		
		ICollection<EnergyType> IDictionary<string, EnergyType>.Values
		{
			get { return m_EnergyDictionary.Values; }
		}

		EnergyType IList<EnergyType>.this[int i]
		{
			get { return energyTypes[i]; }
			set { throw new NotSupportedException("Energy List is read only"); }
		}

		EnergyType IDictionary<string, EnergyType>.this[string key]
		{
			get { return m_EnergyDictionary[key]; }
			set { throw new NotSupportedException("Energy List is read only"); }
		}

		void IList<EnergyType>.Insert(int index, EnergyType item)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		void IList<EnergyType>.RemoveAt(int index)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		void ICollection<EnergyType>.Add(EnergyType item)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		void ICollection<KeyValuePair<string, EnergyType>>.Add(KeyValuePair<string, EnergyType> item)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		void ICollection<KeyValuePair<string, EnergyType>>.Clear()
		{
			throw new NotSupportedException("Energy List is read only");
		}

		bool ICollection<KeyValuePair<string, EnergyType>>.Contains(KeyValuePair<string, EnergyType> item)
		{
			return m_EnergyDictionary.Contains(item);
		}

		void ICollection<KeyValuePair<string, EnergyType>>.CopyTo(KeyValuePair<string, EnergyType>[] array, int arrayIndex)
		{
			m_EnergyDictionary.CopyTo(array, arrayIndex);
		}

		void ICollection<EnergyType>.Clear()
		{
			throw new NotSupportedException("Energy List is read only");
		}

		void ICollection<EnergyType>.CopyTo(EnergyType[] array, int arrayIndex)
		{
			energyTypes.CopyTo(array, arrayIndex);
		}

		bool ICollection<EnergyType>.Remove(EnergyType item)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		public IEnumerator<EnergyType> GetEnumerator()
		{
			return ((IList<EnergyType>) energyTypes).GetEnumerator();
		}

		IEnumerator<KeyValuePair<string, EnergyType>> IEnumerable<KeyValuePair<string, EnergyType>>.GetEnumerator()
		{
			return m_EnergyDictionary.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return energyTypes.GetEnumerator();
		}

		void IDictionary<string, EnergyType>.Add(string key, EnergyType value)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		bool ICollection<KeyValuePair<string, EnergyType>>.Remove(KeyValuePair<string, EnergyType> item)
		{
			throw new NotSupportedException("Energy List is read only");
		}

		bool IDictionary<string, EnergyType>.Remove(string key)
		{
			throw new NotSupportedException("Energy List is read only");
		}
	}
}