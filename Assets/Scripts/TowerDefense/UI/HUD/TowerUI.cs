﻿using TowerDefense.Energy.Data;
using TowerDefense.Level;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.UI.HUD
{
	/// <summary>
	/// Controls the UI objects that draw the tower data
	/// </summary>
	[RequireComponent(typeof(Canvas))]
	public class TowerUI : MonoBehaviour
	{
		/// <summary>
		/// The text object for the name
		/// </summary>
		public Text towerName;

		/// <summary>
		/// The text object for the description
		/// </summary>
		public Text description;
		
		public Text energyDescription;
		public Text upgradeDescription;

		/// <summary>
		/// The attached sell button
		/// </summary>
		public Button sellButton;

		/// <summary>
		/// The attached upgrade button
		/// </summary>
		public Button upgradeButton;
		
		/// <summary>
		/// SM: Energy upgrade buttons
		/// TODO: clumsy and non-scalable atm.  Better to subclass Button to EnergyButton (prefab)
		/// </summary>
		public Button qantmButton;
		public Text qantmUpgradeCost;
		public EnergyType qantmEnergy;		// ref to SO
		public Button lightButton;
		public Text lightUpgradeCost;
		public EnergyType lightEnergy;		// ref to SO
		public Button heatButton;
		public Text heatUpgradeCost;
		public EnergyType heatEnergy;		// ref to SO
		public Button darkButton;
		public Text darkUpgradeCost;
		public EnergyType darkEnergy;		// ref to SO
		public Button electricButton;
		public Text electricUpgradeCost;
		public EnergyType electricEnergy;	// ref to SO

		/// <summary>
		/// Component to display the relevant information of the tower
		/// </summary>
		public TowerInfoDisplay towerInfoDisplay;

		public RectTransform panelRectTransform;

		public GameObject[] confirmationButtons;

		/// <summary>
		/// The main game camera
		/// </summary>
		protected Camera m_GameCamera;

		/// <summary>
		/// The current tower to draw
		/// </summary>
		protected Tower m_Tower;

		/// <summary>
		/// The canvas attached to the gameObject
		/// </summary>
		protected Canvas m_Canvas;

		/// <summary>
		/// Draws the tower data on to the canvas
		/// </summary>
		/// <param name="towerToShow">
		/// The tower to gain info from
		/// </param>
		public virtual void Show(Tower towerToShow)
		{
			if (towerToShow == null)
			{
				return;
			}
			m_Tower = towerToShow;
			//AdjustPosition();		// SM: fixed position. Without panning camera, panel not always fully shown if positioned at selected tower.

			m_Canvas.enabled = true;

			int sellValue = m_Tower.GetSellLevel();
			if (sellButton != null)
			{
				sellButton.gameObject.SetActive(sellValue > 0);
			}
			
			if (upgradeButton != null)
			{
				int upgradeCost = m_Tower.GetCostForNextLevel();		// SM::
				bool canAfford = LevelManager.instance.currency.CanAfford(upgradeCost);

				upgradeButton.interactable = canAfford;
				bool maxLevel = m_Tower.isAtMaxLevel;
				upgradeButton.gameObject.SetActive(!maxLevel);
				
				if (!maxLevel)
				{
					upgradeDescription.text = m_Tower.levels[m_Tower.currentLevel + 1].upgradeDescription.ToUpper();
				}
				
				// SM: energy buttons
				var currentTowerEnergy = m_Tower.levels[m_Tower.currentLevel].energyType;
				
		 		energyDescription.text = (currentTowerEnergy != null) ? currentTowerEnergy.energyDesc : "---";
				
				EnableEnergyButton(qantmButton, maxLevel, canAfford, currentTowerEnergy, qantmEnergy);
				EnableEnergyButton(lightButton, maxLevel, canAfford, currentTowerEnergy, lightEnergy);
				EnableEnergyButton(heatButton, maxLevel, canAfford, currentTowerEnergy, heatEnergy);
				EnableEnergyButton(electricButton, maxLevel, canAfford, currentTowerEnergy, electricEnergy);
				EnableEnergyButton(darkButton, maxLevel, canAfford, currentTowerEnergy, darkEnergy);

				if (qantmUpgradeCost != null)
					qantmUpgradeCost.text = upgradeCost.ToString();
				if (lightUpgradeCost != null)
					lightUpgradeCost.text = upgradeCost.ToString();
				if (heatUpgradeCost != null)
					heatUpgradeCost.text = upgradeCost.ToString();
				if (electricUpgradeCost != null)
					electricUpgradeCost.text = upgradeCost.ToString();
				if (darkUpgradeCost != null)
					darkUpgradeCost.text = upgradeCost.ToString();
			}
			
			LevelManager.instance.currency.currencyChanged += OnCurrencyChanged;
			towerInfoDisplay.Show(towerToShow);
			foreach (var button in confirmationButtons)
			{
				button.SetActive(false);
			}
		}

		/// <summary>
		/// Hides the tower info UI and the radius visualizer
		/// </summary>
		public virtual void Hide()
		{
			m_Tower = null;
			if (GameUI.instanceExists)
			{
				GameUI.instance.HideRadiusVisualizer();
			}
			m_Canvas.enabled = false;
			LevelManager.instance.currency.currencyChanged -= OnCurrencyChanged;
		}
		
		/// <summary>
		/// Upgrades the tower through <see cref="GameUI"/>
		/// </summary>
		public void UpgradeButtonClick()
		{
			GameUI.instance.UpgradeSelectedTower(null); 
		}
		
		
		/// <summary>
		/// SM: enables/colours energy button according to its EnergyType and the TowerLevel's current EnergyType
		/// TODO: clumsy and non-scalable atm.  Better to subclass Button to EnergyButton (prefab)
		/// </summary>
		/// <param name="energyButton">Energy button.</param>
		/// <param name="maxLevel">If set to <c>true</c> max level.</param>
		/// <param name="canAfford">If set to <c>true</c> can afford.</param>
		/// <param name="towerEnergy">Tower energy.</param>
		/// <param name="buttonEnergy">Button energy.</param>
		private void EnableEnergyButton(Button energyButton, bool maxLevel, bool canAfford, EnergyType towerEnergy, EnergyType buttonEnergy)
		{
			//bool defaultEnergy = towerEnergy == null || towerEnergy.energyForm == EnergyForm.Default;

			//Debug.Log("EnableEnergyButton: towerEnergy = " + (towerEnergy == null ? "null" : towerEnergy.energyForm.ToString()) + ", buttonEnergy = " + buttonEnergy.energyForm + ", canAfford = " + canAfford);
			
			// disable button if can't afford or tower level already this energy
			//energyButton.interactable = canAfford && (defaultEnergy || towerEnergy.energyForm != buttonEnergy.energyForm);
			
			// disable energy button if can't afford
			energyButton.interactable = canAfford;

			// hide button if can't upgrade tower any more
			energyButton.gameObject.SetActive(!maxLevel);
			
			// set button colour according to energy
			energyButton.GetComponent<Image>().color = buttonEnergy.energyColour;
		}

		/// <summary>
		/// SM: Upgrades the tower with EnergyType through <see cref="GameUI"/>
		/// </summary>
		public void UpgradeEnergyButtonClick(EnergyType energyType)
		{
			GameUI.instance.UpgradeSelectedTower(energyType);
		}

		/// <summary>
		/// Sells the tower through <see cref="GameUI"/>
		/// </summary>
		public void SellButtonClick()
		{
			GameUI.instance.SellSelectedTower();
		}

		/// <summary>
		/// Get the text attached to the buttons
		/// </summary>
		protected virtual void Awake()
		{
			m_Canvas = GetComponent<Canvas>();
		}

		/// <summary>
		/// Fires when tower is selected/deselected
		/// </summary>
		/// <param name="newTower"></param>
		protected virtual void OnUISelectionChanged(Tower newTower)
		{
			if (newTower != null)
			{
				Show(newTower);
			}
			else
			{
				Hide();
			}
		}

		/// <summary>
		/// Subscribe to mouse button action
		/// </summary>
		protected virtual void Start()
		{
			m_GameCamera = Camera.main;
			m_Canvas.enabled = false;
			if (GameUI.instanceExists)
			{
				GameUI.instance.selectionChanged += OnUISelectionChanged;
				GameUI.instance.stateChanged += OnGameUIStateChanged;
			}
		}

		/// <summary>
		/// Adjust position when the camera moves
		/// </summary>
		protected virtual void Update()
		{
			//AdjustPosition();
		}

		/// <summary>
		/// Unsubscribe from currencyChanged
		/// </summary>
		protected virtual void OnDisable()
		{
			if (LevelManager.instanceExists)
			{
				LevelManager.instance.currency.currencyChanged -= OnCurrencyChanged;
			}
		}

		/// <summary>
		/// Adjust the position of the UI
		/// </summary>
		protected void AdjustPosition()
		{
			if (m_Tower == null)
			{
				return;
			}
			Vector3 point = m_GameCamera.WorldToScreenPoint(m_Tower.position);
			point.z = 0;
			panelRectTransform.transform.position = point;
		}

		/// <summary>
		/// Fired when the <see cref="GameUI"/> state changes
		/// If the new state is <see cref="GameUI.State.GameOver"/> we need to hide the <see cref="TowerUI"/>
		/// </summary>
		/// <param name="oldState">The previous state</param>
		/// <param name="newState">The state to transition to</param>
		protected void OnGameUIStateChanged(GameUI.State oldState, GameUI.State newState)
		{
			if (newState == GameUI.State.GameOver)
			{
				Hide();
			}
		}

		/// <summary>
		/// Check if player can afford upgrade on currency changed
		/// </summary>
		void OnCurrencyChanged()
		{
			if (m_Tower != null && upgradeButton != null)
			{
				bool canAfford = LevelManager.instance.currency.CanAfford(m_Tower.GetCostForNextLevel());
				
				upgradeButton.interactable = canAfford;
				
				// SM: energy buttons
				qantmButton.interactable = canAfford;
				lightButton.interactable = canAfford;
				heatButton.interactable = canAfford;
				electricButton.interactable = canAfford;
				darkButton.interactable = canAfford;
			}
		}

		/// <summary>
		/// Unsubscribe from GameUI selectionChanged and stateChanged
		/// </summary>
		void OnDestroy()
		{
			if (GameUI.instanceExists)
			{
				GameUI.instance.selectionChanged -= OnUISelectionChanged;
				GameUI.instance.stateChanged -= OnGameUIStateChanged;
			}
		}
	}
}